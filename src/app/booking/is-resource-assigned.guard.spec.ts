import { TestBed } from '@angular/core/testing';

import { IsResourceAssignedGuard } from './is-resource-assigned.guard';

describe('IsResourceAssignedGuard', () => {
  let guard: IsResourceAssignedGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(IsResourceAssignedGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});

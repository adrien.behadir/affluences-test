import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  RouterStateSnapshot,
  UrlTree
} from '@angular/router';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

import * as Helpers from 'src/app/helpers';
import { BookingService } from './booking.service';

@Injectable({
  providedIn: 'root'
})
export class IsResourceAssignedGuard implements CanActivate {

  constructor (
    private _router: Router,
    private _bookingService: BookingService
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if ( !Helpers.isDefined( this._bookingService.resource )) {
      this._router.navigateByUrl( 'bookings' );
      return false;
    }
    return true;
  }
  
}

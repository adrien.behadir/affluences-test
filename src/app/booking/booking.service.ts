import { Injectable } from '@angular/core';

import * as Helpers from 'src/app/helpers'
import {
  Resource,
  ReservationCandidate
} from 'src/app/models';

@Injectable({
  providedIn: 'root'
})
export class BookingService {

  private _resource: Resource | null;
  private _reservationCandidate: ReservationCandidate | null;

  constructor() {
    this._resource = null;
    this._reservationCandidate = null;
  }

  get resource(): Resource | null {
    return this._resource;
  }

  set resource( resource: Resource | null ) {
    if ( Helpers.isDefined( resource )) {
      this._resource = resource;
    } else {
      throw new Error( `Operation not allowed for ${typeof resource}:${resource}. Did you want to use clearDatas()?` );
    }
  }

  get reservationCandidate(): ReservationCandidate | null {
    return this._reservationCandidate;
  }

  set reservationCandidate( reservationCandidate: ReservationCandidate | null ) {
    if ( Helpers.isDefined( reservationCandidate )) {
      this._reservationCandidate = reservationCandidate;
    } else {
      throw new Error( `Operation not allowed for ${typeof reservationCandidate}:${reservationCandidate}. Did you want to use clearDatas()?` );
    }
  }

  public clearDatas() {
    this._resource = null;
    this._reservationCandidate = null;
  }
}

import {
  Component,
  OnInit
} from '@angular/core';
import {
  FormControl,
  FormGroup,
  Validators
} from '@angular/forms';
import { Router } from '@angular/router';
import {
  first,
  Subject,
  takeUntil
} from 'rxjs';

import { AccountService } from 'src/app/services/account/account.service';
import { ReservationService } from '../api/services/reservation/reservation.service';
import { NotificationMessageType } from '../components/snackbar-notification/models';
import { SnackbarNotificationService } from '../components/snackbar-notification/services/snackbar-notification.service';
import {
  Reservation,
  ReservationCandidate
} from '../models';
import { BookingService } from './booking.service';

@Component({
  selector: 'app-booking',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.scss']
})
export class BookingComponent implements OnInit {

  formGroup: FormGroup;
  saveFormControl: FormControl;
  private destroy$ = new Subject();

  constructor(
    private _router: Router,
    public bookingService: BookingService,
    public accountService: AccountService,
    public reservationService: ReservationService,
    private _notificationService: SnackbarNotificationService,
  ) {
    this.formGroup = new FormGroup({
      'firstName': new FormControl(
        accountService.account.firstName,
        [
          Validators.required
        ]
      ),
      'lastName': new FormControl(
        accountService.account.lastName,
        [
          Validators.required
        ]
      ),
      'email': new FormControl(
        accountService.account.email,
        [
          Validators.required,
          Validators.email
        ]
      ),
      'phone': new FormControl(
        accountService.account.phone,
        [
          Validators.required,
          Validators.pattern( /^(\+\d{2}\s)?\d{9,10}$/ )  // E.123
        ]
      ),
      'tos': new FormControl(
        false,
        [
          Validators.requiredTrue
        ]
      ),
    });
    this.saveFormControl = new FormControl( this.accountService.hasDatas());
  }

  ngOnInit(): void {
    this.formGroup.valueChanges
    .pipe(
      takeUntil( this.destroy$ )
    )
    .subscribe(( formControlValues: I_FormControlValues ) => {
      if ( this.saveFormControl.value ) {
        this.accountService.saveAccount({
          email: this.formGroup.value[ 'email' ],
          phone: this.formGroup.value[ 'phone' ],
          lastName: this.formGroup.value[ 'lastName' ],
          firstName: this.formGroup.value[ 'firstName' ],
        })
      } else {
        this.accountService.clearAccount();
      }
    });
    this.saveFormControl.valueChanges
    .pipe(
      takeUntil( this.destroy$ )
    )
    .subscribe(( save: boolean ) => {
      if ( save ) {
        this.accountService.saveAccount({
          email: this.formGroup.value[ 'email' ],
          phone: this.formGroup.value[ 'phone' ],
          lastName: this.formGroup.value[ 'lastName' ],
          firstName: this.formGroup.value[ 'firstName' ],
        })
      } else {
        this.accountService.clearAccount();
      }
    });
  }

  ngOnDestroy(){
    this.destroy$.next( null );
    this.destroy$.complete(); 
  }

  navigateToBookings() {
    this.bookingService.clearDatas();
    this._router.navigateByUrl( '/bookings' );
  }

  public onSubmit() {
    this.reservationService.create( this.bookingService.reservationCandidate as ReservationCandidate )
    .pipe(
      first()
    )
    .subscribe(( reservation: Reservation ) => {
      this._notificationService.openSnackBar(
        {
          icon: 'check_circle',
          message: `Reservation:${reservation.id} créée le ${reservation.date}.`,
          type: NotificationMessageType.SUCCESS,
          duration: 3000
        },
        'CLOSE_ANYWAY'
      );
      this.navigateToBookings();
    });
  }
}

interface I_FormControlValues {
  firstName: string;
  lastName: string;
  email: string;
  phone: string;
  tos: string;
}

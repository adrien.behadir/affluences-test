import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpErrorResponse
} from '@angular/common/http';
import {
  catchError,
  finalize,
  Observable,
  throwError,
  map
} from 'rxjs';
import { environment } from 'src/environments/environment';
import {
  I_ReservationAPI,
  Reservation,
  ReservationCandidate
} from 'src/app/models';
import { SnackbarNotificationService } from 'src/app/components/snackbar-notification/services/snackbar-notification.service';
import { NotificationMessageType } from 'src/app/components/snackbar-notification/models';

@Injectable({
  providedIn: 'root'
})
export class ReservationService {

  busy: boolean;

  constructor( 
    private http: HttpClient,
    private _notificationService: SnackbarNotificationService
  ) {
    this.busy = false;
  }

  public create( reservationCandidate: ReservationCandidate ): Observable<Reservation> {
    this.busy = true;
    return this.http.post<I_ReservationAPI>(
      `${ environment.api.address }/reservation`,
      JSON.stringify( reservationCandidate )
    )
    .pipe(
      catchError (( error: HttpErrorResponse ) => {
        this._notificationService.openSnackBar(
          {
            icon: 'error',
            message: `${error.status}:${error.statusText}`,
            type: NotificationMessageType.ERROR,
            duration: 3000
          },
          'CLOSE_ANYWAY'
        ); 
        return throwError(() => error);
      }),
      finalize(() => this.busy = false ),
      map(( reservation: I_ReservationAPI ) => {
        return new Reservation({
          date: new Date( reservation.date ),
          id: reservation.id,
          resourceId: reservation.resourceId
        });
      })
    );
  }
}

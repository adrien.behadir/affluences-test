import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpErrorResponse,
  HttpParams
} from '@angular/common/http';
import {
  catchError,
  finalize,
  Observable,
  throwError,
  map
} from 'rxjs';
import { environment } from 'src/environments/environment';
import {
  I_Resource,
  Resource
} from 'src/app/models/resource.model';

import { SnackbarNotificationService } from 'src/app/components/snackbar-notification/services/snackbar-notification.service';
import { NotificationMessageType } from 'src/app/components/snackbar-notification/models';

@Injectable({
  providedIn: 'root'
})
export class ResourceAPIService {

  busy: boolean;

  constructor(
    private http: HttpClient,
    private _notificationService: SnackbarNotificationService
  ) {
    this.busy = false;
  }

  public available(
    resource: Resource,
    filters: I_QueryParams
  ): Observable<I_BodyResponse> {
    const params = new HttpParams().appendAll({
      'datetime': filters.datetime
    });
    
    this.busy = true;
    return this.http.get<I_BodyResponse>(
      `${ environment.api.address }/resource/${ resource.id }/available`,
      { params }
    )
    .pipe(
      catchError (( error: HttpErrorResponse ) => {
        this._notificationService.openSnackBar(
          {
            icon: 'error',
            message: `${error.status}:${error.statusText}`,
            type: NotificationMessageType.ERROR,
            duration: 3000
          },
          'CLOSE_ANYWAY'
        ); 
        return throwError(() => error);
      }),
      finalize(() => this.busy = false )
    );
  }

  public get( filters: I_Resource ): Observable<Resource[]> {
    const params = new HttpParams().appendAll({
      'id': filters.id
    });

    return this.http.get<I_Resource[]>(
      `${ environment.api.address }/resources`,
      { params }
    )
    .pipe(
      catchError (( error: HttpErrorResponse ) => {
        this._notificationService.openSnackBar(
          {
            icon: 'error',
            message: `${error.status}:${error.statusText}`,
            type: NotificationMessageType.ERROR,
            duration: 3000
          },
          'CLOSE_ANYWAY'
        ); 
        return throwError(() => error);
      }),
      finalize(() => this.busy = false ),
      map((res: I_Resource[] ) => {
        const output: Resource[] = res.map(( resource: I_Resource ) => new Resource( resource ));
        return output;
      })
    );
  }
}

export interface I_QueryParams {
  datetime: string  // ISO 8601
}

export interface I_BodyResponse {
  available: boolean;
}

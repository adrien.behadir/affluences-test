import { TestBed } from '@angular/core/testing';

import { ResourceAPIService } from './resource.service';

describe('ApiService', () => {
  let service: ResourceAPIService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ResourceAPIService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

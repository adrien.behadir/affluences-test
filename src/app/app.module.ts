import { NgModule } from '@angular/core';
import {
  HttpClientModule,
  HTTP_INTERCEPTORS
} from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { SnackbarNotificationModule } from './components/snackbar-notification/snackbar-notification.module';
import { ResourceInterceptor } from './interceptors/resource/resource.interceptor';
import { ReservationInterceptor } from './interceptors/reservation/reservation.interceptor';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    SnackbarNotificationModule,
    HttpClientModule
  ],
  exports: [
    SnackbarNotificationModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ResourceInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ReservationInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

import {
  Component,
  OnDestroy,
  OnInit
} from '@angular/core';
import {
  FormControl,
  FormGroup,
  Validators
} from '@angular/forms';
import { Router } from '@angular/router';

import {
  Subject,
  first
} from 'rxjs';
import {
  finalize,
  takeUntil
} from 'rxjs/operators'

import {
  I_BodyResponse,
  ResourceAPIService
} from '../api/services/resource/resource.service';
import { BookingService } from '../booking/booking.service';
import { NotificationMessageType } from '../components/snackbar-notification/models';
import { SnackbarNotificationService } from '../components/snackbar-notification/services/snackbar-notification.service';
import { I_Resource, Resource } from '../models/resource.model';
import * as Helpers from 'src/app/helpers'
import { ReservationCandidate } from '../models';

@Component({
  selector: 'app-bookings',
  templateUrl: './bookings.component.html',
  styleUrls: ['./bookings.component.scss']
})
export class BookingsComponent implements OnInit, OnDestroy {

  public formGroup: FormGroup;
  public resource: Resource;        // The selected Resource for the availability check operation.
  public dataLoaded: boolean;
  private destroy$ = new Subject();

  constructor(
    private _notificationService: SnackbarNotificationService,
    public resourceService: ResourceAPIService,
    private _router: Router,
    private _bookingService: BookingService,
  ) {
    this.dataLoaded = false;
    this.formGroup = new FormGroup({
      'date': new FormControl(
        '',
        [
          Validators.required
        ]
      ),
      'hour': new FormControl(
        '',
        [
          Validators.required
        ]
      ),
    });
  }

  ngOnInit(): void {
    this.formGroup.valueChanges
    .pipe(
      takeUntil( this.destroy$ )
    )
    .subscribe(( formControlValues: formControlValues ) => {
      // console.log( formControlValues );
    })

    const filters: I_Resource = { id: '1337' };
    this.resourceService.get( filters )    // Hard assigned since no project specifications for resource selection.
    .pipe(
      first(),
      finalize(() => this.dataLoaded = true )
    )
    .subscribe(( resources: Resource[] ) => {
      const resource: Resource | undefined = resources.find(( resource: Resource ) => resource.id === filters.id );
      if ( Helpers.isDefined( resource )) {
        this.resource = resource as Resource;
      } else {
        this._notificationService.openSnackBar(
          {
            icon: 'error_outline',
            message: `No resource found with filters ${JSON.stringify( filters )}`,
            type: NotificationMessageType.INFO,
            duration: 3000
          },
          'CLOSE_ANYWAY'
        );
      }
    })
  }

  ngOnDestroy(){
    this.destroy$.next( null );
    this.destroy$.complete(); 
  }

  public onSubmit() {
    this.resourceService.available(
      this.resource,
      {
        datetime: `${this.formGroup.value[ 'date' ]}T${ this.formGroup.value[ 'hour' ]}`
      }
    )
    .pipe(
      first()
    )
    .subscribe(( res: I_BodyResponse ) => {
      this._notificationService.openSnackBar(
        {
          icon: res.available ? 'check_circle' : 'cancel',
          message: res.available ? 'Créneau disponible.' : 'Créneau indisponible !',
          type: res.available ? NotificationMessageType.INFO : NotificationMessageType.WARNING,
          duration: 3000
        },
        'CLOSE_ANYWAY'
      );
      if ( res.available ) {
        this._navigateToBooking();
      }
    });
  }

  private _navigateToBooking() {
    this._bookingService.reservationCandidate = new ReservationCandidate({
      date: new Date( `${this.formGroup.value[ 'date' ]}T${ this.formGroup.value[ 'hour' ]}` ),
      resourceId: this.resource.id
    });
    this._bookingService.resource = this.resource;
    this._router.navigateByUrl( `/bookings/${ this.resource.id }` );
  }
}

interface formControlValues {
  'date': string;
  'hour': string;
}

import { Injectable } from '@angular/core';
import { I_Account } from 'src/app/models';
import * as Helpers from 'src/app/helpers';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  public account: I_Account;

  constructor() {
    this.account = {
      email: sessionStorage.getItem( 'email' ),
      firstName: sessionStorage.getItem( 'firstName' ),
      lastName: sessionStorage.getItem( 'lastName' ),
      phone: sessionStorage.getItem( 'phone' )
    }
  }

  public saveAccount( account: I_Account ) {
    if ( Helpers.isDefined( account.email )) {
      sessionStorage.setItem( 'email', account.email as string );
    } else {
      sessionStorage.removeItem( 'email' );
    }
    if ( Helpers.isDefined( account.firstName )) {
      sessionStorage.setItem( 'firstName', account.firstName as string );
    } else {
      sessionStorage.removeItem( 'firstName' );
    }
    if ( Helpers.isDefined( account.lastName )) {
      sessionStorage.setItem( 'lastName', account.lastName as string );
    } else {
      sessionStorage.removeItem( 'lastName' );
    }
    if ( Helpers.isDefined( account.phone )) {
      sessionStorage.setItem( 'phone', account.phone as string );
    } else {
      sessionStorage.removeItem( 'phone' );
    }
    this._loadAccount();
  }

  private _loadAccount() {
    this.account = {
      email: sessionStorage.getItem( 'email' ),
      firstName: sessionStorage.getItem( 'firstName' ),
      lastName: sessionStorage.getItem( 'lastName' ),
      phone: sessionStorage.getItem( 'phone' )
    }
  }

  public clearAccount() {
    sessionStorage.clear();
    this._loadAccount();
  }

  public hasDatas() {
    return sessionStorage.getItem( 'email' ) ||
      sessionStorage.getItem( 'firstName' ) ||
      sessionStorage.getItem( 'lastName' ) ||
      sessionStorage.getItem( 'phone' );
  }

}

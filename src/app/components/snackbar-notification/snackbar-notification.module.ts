import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatSnackBarModule } from '@angular/material/snack-bar';

import { SnackbarNotificationComponent } from './snackbar-notification.component';
import { SnackbarNotificationService } from './services/snackbar-notification.service';

@NgModule({
    declarations: [
        SnackbarNotificationComponent
    ],
    imports: [
        CommonModule,
        MatIconModule,
        MatButtonModule,
        MatProgressBarModule,
        MatSnackBarModule
    ],
    exports: [
        SnackbarNotificationComponent,
        MatIconModule
    ],
    providers: [
        SnackbarNotificationService
    ],
    entryComponents: [
        SnackbarNotificationComponent
    ],
})

export class SnackbarNotificationModule {}

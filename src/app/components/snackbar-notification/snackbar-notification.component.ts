import {
    ChangeDetectorRef,
    Component,
    Inject,
    OnDestroy,
    OnInit,
} from '@angular/core';
import {
    MAT_SNACK_BAR_DATA,
    MatSnackBarRef
} from '@angular/material/snack-bar';

import {
    NotificationMessageType,
    ProgressBar,
    SnackBarData
} from './models';

@Component({
    selector: 'polynom-snackbar-notification',
    styleUrls: ['./snackbar-notification.component.scss'],
    templateUrl: './snackbar-notification.component.html'
})
export class SnackbarNotificationComponent implements OnInit, OnDestroy {

    public _progressBar: ProgressBar;

    /**
     * Class constructor.
     * 
     * @param {SnackbarNotificationService} _snackbarNotificationService - SnackbarNotificationService .
     * @param {ChangeDetectorRef} ref - Call render refresh.
     */
    constructor (
        @Inject( MAT_SNACK_BAR_DATA ) public data: SnackBarData,
        private ref: ChangeDetectorRef,
        private snackBar: MatSnackBarRef<SnackbarNotificationComponent>
    ) {}

    /**
     * Called when component is initialised.
     */
    ngOnInit (): void {
        if ( this.data.duration !== undefined ) {
            this._progressBar = new ProgressBar({ duration: this.data.duration });
            this._progressBar.startAnimation( this.ref );
        }
    }

    /**
     * Called when component is destroy.
     */
    ngOnDestroy (): void {
        if ( this._progressBar ) {
            this._progressBar.endAnimation();
        }
    }

    /**
     * Close.
     * 
     * @public
     */
    public close (): void {
        this.snackBar.dismiss();
    }

    /**
     * Generate message class based on message type.
     * 
     * @private
     * @returns {string} Generated message class.
     */
    public getClass (): string {
        let messageClass = '';
        switch ( this.data.type ) {
            case NotificationMessageType.SUCCESS:
                messageClass = 'fg-green';
                break;
            case NotificationMessageType.WARNING:
                messageClass = 'fg-orange';
                break;
            case NotificationMessageType.ERROR:
                messageClass = 'fg-warn';
                break;
            case NotificationMessageType.INFO:
                messageClass = 'fg-accent';
                break;
            default:
                messageClass = 'fg-accent';
        }
        return messageClass;
    }
}

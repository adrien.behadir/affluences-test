import { NotificationMessageType } from ".";

/**
 * TODO comment
 */
export interface SnackBarData {
    duration: number | undefined;   // Time in ms for display duration, or infinite display with undefined value
    icon: string;                   // TODO comment
    message: string;                // TODO comment
    type: NotificationMessageType;  // TODO comment
}

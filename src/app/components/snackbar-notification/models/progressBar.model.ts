import { ChangeDetectorRef } from '@angular/core';


/**
 * Class ProgressBar.
 */
export class ProgressBar {
    
    private readonly REFRESH_FREQUENCY: number;

    private duration: number;               // TODO comment
    public value: number;                   // TODO comment
    private _timerInterval: NodeJS.Timer;   // TODO comment

    /**
     * Constructor whith dependencies injection.
     * 
     * @public
     */
    public constructor ( config: {
        duration: number;
    } ) {

        this.duration = config.duration
        this.REFRESH_FREQUENCY = 50;
        this.value             = 100;

    }

    /**
     * Starts an animation.
     * 
     * @public
     * @param {ChangeDetectorRef} ref - Call for refresh component render.
     */
    public startAnimation ( ref: ChangeDetectorRef ): void {

        const increment     = 100 / ( this.duration / this.REFRESH_FREQUENCY )
        this._timerInterval = setInterval(
            () => {
                this.value -= increment;
                ref.markForCheck();
                if( this.value === 0 ) {
                    clearInterval( this._timerInterval );
                }
            }, this.REFRESH_FREQUENCY
        );
    
    }

    /**
     * End an animation.
     * 
     * @public
     */
    public endAnimation (): void {

        clearInterval( this._timerInterval );
    
    }
}

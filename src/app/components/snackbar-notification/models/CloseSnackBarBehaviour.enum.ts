/**
 * TODO comment
 */
export enum E_CloseSnackBarBehaviour {
    CLOSE_UNDEFINED_TIME_VALUE_SNACKBAR,
    CLOSE_DEFINED_TIME_VALUE_SNACKBAR,
    CLOSE_ANYWAY
}

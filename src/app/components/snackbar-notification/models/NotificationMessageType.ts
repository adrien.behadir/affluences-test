/**
 * The notification type
 */
export enum NotificationMessageType {
    ERROR = 'ERROR',
    INFO = 'INFO',
    SUCCESS = 'SUCCESS',
    WARNING = 'WARNING'
}

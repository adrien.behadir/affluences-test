import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { IsResourceAssignedGuard } from 'src/app/booking/is-resource-assigned.guard'

const routes: Routes = [
  {
    path: 'bookings',
    loadChildren: () => import('./bookings/bookings.module').then(m => m.BookingsModule)
  },
  {
    path: 'bookings/:resourceId',
    loadChildren: () => import('./booking/booking.module').then(m => m.BookingModule),
    canActivate: [
      IsResourceAssignedGuard
    ]
  },
  {
    path: '',
    redirectTo: '/bookings',
    pathMatch: 'full'
  },
  // { path: '**', component: PageNotFoundComponent },  // Wildcard route for a 404 page
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }

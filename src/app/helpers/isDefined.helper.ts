/**
 * Check if the value is defined
 * 
 * @param {string | undefined | number | boolean | object | null} value - value to check
 * @returns {boolean} - isDefined result
 */
export function isDefined( value: string | undefined | number | boolean | object | null ): boolean {
    switch ( typeof value ) {
        case 'boolean':
        case 'number':
        case 'string': {
            return true;
        };
        case 'undefined': {
            return false;
        };
        case 'object': {
            if ( value === null ) {
                return false;
            }
            return true;
        };
        default: {
            throw new Error( `unsupported type of ${typeof value} into isDefined( value: string | undefined | number | boolean | object | null ) => boolean` );
        }
    }
}

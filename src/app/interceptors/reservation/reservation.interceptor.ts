import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse
} from '@angular/common/http';
import {
  delay,
  Observable,
  of
} from 'rxjs';
import { environment } from 'src/environments/environment'
import { I_ReservationAPI } from 'src/app/models';

@Injectable()
export class ReservationInterceptor implements HttpInterceptor {

  constructor() {}

  intercept(request: HttpRequest<string>, next: HttpHandler): Observable<HttpEvent<I_ReservationAPI>> {
    if (
      request.method === 'POST' &&
      request.url === `${ environment.api.address }/reservation`
    ) {
      return of(
        new HttpResponse({
          status: 201,
          body: {
            id: '1',
            resourceId: JSON.parse( request.body as string )[ 'resourceId' ],
            date: JSON.parse( request.body as string )[ 'date' ]
          }
        })
      ).pipe(
        delay( 2800 )
      );
    } else {
      return next.handle(request);
    }
  }
}

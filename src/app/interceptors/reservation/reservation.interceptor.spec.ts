import { TestBed } from '@angular/core/testing';

import { ReservationInterceptor } from './reservation.interceptor';

describe('ReservationInterceptor', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      ReservationInterceptor
      ]
  }));

  it('should be created', () => {
    const interceptor: ReservationInterceptor = TestBed.inject(ReservationInterceptor);
    expect(interceptor).toBeTruthy();
  });
});

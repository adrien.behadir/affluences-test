import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse
} from '@angular/common/http';
import {
  delay,
  Observable,
  of
} from 'rxjs';
import { I_Resource } from 'src/app/models/resource.model';
import { environment } from 'src/environments/environment'

@Injectable()
export class ResourceInterceptor implements HttpInterceptor {

  constructor() {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    if (
      request.method === 'GET' &&
      request.url === `${ environment.api.address }/resources`
    ) {
      return of(
        new HttpResponse({ status: 200, body: resources.filter(( resource: I_Resource ) => resource.id === request.params.get( 'id' ) )})
      ).pipe(
        delay( 2800 )
      );
    } else {
      return next.handle(request);
    }
  }
}

const resources: I_Resource[] = [
  {
    id: '1336'
  },
  {
    id: '1337'
  },
]

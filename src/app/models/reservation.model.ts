export class ReservationCandidate implements I_ReservationCandidate {
    date: Date;
    resourceId: string;

    constructor( reservationCandidate: I_ReservationCandidate ) {
        this.date = reservationCandidate.date;
        this.resourceId = reservationCandidate.resourceId;
    }
}

export class Reservation extends ReservationCandidate implements I_Reservation {
    id: string;

    constructor( reservation: I_Reservation ) {
        super( reservation );
        this.id = reservation.id;
    }
}

export interface I_Reservation extends I_ReservationCandidate {
    id: string;
}

export interface I_ReservationCandidate {
    resourceId: string;
    date: Date;
}

export interface I_ReservationAPI extends I_ReservationCandidateAPI {
    id: string;
}

export interface I_ReservationCandidateAPI {
    resourceId: string;
    date: string;
}

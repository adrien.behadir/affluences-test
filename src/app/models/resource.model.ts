export class Resource {
    id: string;

    constructor( resource: I_Resource ) {
        this.id = resource.id;
    }
}

export interface I_Resource {
    id: string;
}

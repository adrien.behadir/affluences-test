export interface I_Account {
    firstName: string | null;
    lastName: string | null;
    phone: string | null;
    email: string | null;
}
